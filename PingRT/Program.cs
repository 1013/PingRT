﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace PingRT
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            ConfigINI CheckINI = new ConfigINI();
            CheckINI.IniFiles();
            NotifyEvent NotifyIcon = new NotifyEvent();
            NotifyIcon.Display();
            System.Threading.Thread Alarm = new System.Threading.Thread(Policy.AlarmPolicy);
            Alarm.Start();
            Pinger Ping = new Pinger();
            FlowRate Meter = new FlowRate();
            Application.Run();
        }
    }
}
