﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;  

namespace PingRT
{
    static class KBEvent
    {
        [DllImport("user32.dll", EntryPoint = "keybd_event")]

        public static extern void keybd_event(
        byte bVk, //虚拟键值  
        byte bScan,// 一般为0  
        int dwFlags, //这里是整数类型 0 为按下，2为释放  
        int dwExtraInfo //这里是整数类型 一般情况下设成为0  
        );

        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        public static extern short GetKeyState(int keyCode);

        public static void pressScroll()
        {
            keybd_event(145, 0, 0, 0);
            keybd_event(145, 0, 2, 0);
        }

        public static bool getScrollStatus()
        {
            bool status;
            if ((((ushort)GetKeyState(0x91)) & 0xffff) != 0)
                status = true;
            else
                status = false;
            return status;
        }

    }
}
