﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PingRT
{
    public class AlarmPolicyArgs
    {
        public delegate void AlarmPolicy(object sender, EventArgs e);
        public event AlarmPolicy AlarmPolicyKBEvent;
        public event AlarmPolicy AlarmPolicyIconEvent;

        public void AlarmPolicyRun()
        {
            
            while(true)
            {
                int ErrorCounter = 0;

                foreach (object Counter in Que.DisplayArry())
                {
                    System.Diagnostics.Debug.Write(Counter + " | ");
                    
                    if (Convert.ToInt16(Counter) == 0)
                        ErrorCounter++;
                }
                System.Diagnostics.Debug.WriteLine("count:" + ErrorCounter + "|flow:" + FlowRate.GetRecievedByte().ToString() + "|limit:" + Pinger.ConfigNodeHost[1].ToString());
                System.Diagnostics.Debug.WriteLine("");
                if (Pinger.ConfigNodeOption[2].ToString() == "true")
                {
                    if (!NetworkStatus.IsAvailable)
                        AlarmPolicyKBEvent(this, new EventArgs());

                    else if (ErrorCounter >= 3)
                        AlarmPolicyKBEvent(this, new EventArgs());
                }
                
                else
                    if (KBEvent.getScrollStatus())
                    KBEvent.pressScroll();

                if (Pinger.ConfigNodeOption[3].ToString() == "true")
                {
                    if (!NetworkStatus.IsAvailable)
                        AlarmPolicyIconEvent(this, new EventArgs());

                    else if (ErrorCounter >= 3)
                        AlarmPolicyIconEvent(this, new EventArgs());
                }
                
                else
                    NotifyEvent.DefaultIcon();
                System.Threading.Thread.Sleep(1000);
            }
        }
    }
}
