﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Configuration;
using System.Diagnostics;
using System.IO.Compression;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace PingRT
{
        public class Utils
        {
            [DllImport("kernel32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool SetProcessWorkingSetSize(IntPtr process,
                UIntPtr minimumWorkingSetSize, UIntPtr maximumWorkingSetSize);


            /// <summary>
            /// 手动释放内存
            /// </summary>
            /// <param name="removePages"></param>
            public static void ReleaseMemory(bool removePages)
            {
                GC.Collect(GC.MaxGeneration);
                GC.WaitForPendingFinalizers();
                if (removePages)
                {
                    SetProcessWorkingSetSize(Process.GetCurrentProcess().Handle,
                        (UIntPtr)0xFFFFFFFF, (UIntPtr)0xFFFFFFFF);
                }
            }
           
            /// <summary>
            /// 是否为ip
            /// </summary>
            /// <param name="ip"></param>
            /// <returns></returns>
            public static bool IsIP(string ip)
            {
                return Regex.IsMatch(ip, @"^((2[0-4]\d|25[0-5]|[01]?\d\d?)\.){3}(2[0-4]\d|25[0-5]|[01]?\d\d?)$");
            }

            /// <summary>
            /// 是否为域名
            /// </summary>
            /// <param name="ip"></param>
            /// <returns></returns>
            public static bool IsDomain(string domain)
            {
                return Regex.IsMatch(domain, @"[A-Za-z0-9_]+(\.[A-Za-z0-9_]+)+");
            }


            /// <summary>
            /// 是否为正整数
            /// </summary>
            /// <param name="ip"></param>
            /// <returns></returns>
            public static bool IsInt(string Int)
            {
                return Regex.IsMatch(Int, @"^[1-9]\d*");
            }

            /// <summary>
            /// 替换指定的字符串
            /// </summary>
            /// <param name="originalStr">原字符串</param>
            /// <param name="oldStr">旧字符串</param>
            /// <param name="newStr">新字符串</param>
            /// <returns></returns>
            public static string ReplaceStr(string originalStr, string oldStr, string newStr)
            {
                if (string.IsNullOrEmpty(oldStr))
                {
                    return "";
                }
                return originalStr.Replace(oldStr, newStr);
            }

        }
}