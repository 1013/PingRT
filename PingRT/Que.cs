﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PingRT
{
    class Que
    {

        public static Queue q = new Queue();

        /// <summary>
        /// 向队列添加元素
        /// </summary>
        /// <param name="x"></param>
        public static void AddValue(long x)
        {
            q.Enqueue(x);
            if (q.Count - 10 > 0)
                Que.DelValue();
        }

        /// <summary>
        /// 删除队列元素，先进先出原则,一次一个
        /// </summary>
        /// <param name="x"></param>
        public static void DelValue()
        {
            q.Dequeue();

        }

        public static void DelAll()
        {
            q.Clear();
        }

        /// <summary>
        /// 转为数组输出
        /// </summary>
        public static object[] DisplayArry()
        {
            object[] arrQue = q.ToArray();
            return arrQue;
        }

        /// <summary>
        /// 输出队列输出
        /// </summary>
        /// <returns></returns>
        public static Queue DispalQue()
        {
            return q;
        }

    }
}
