﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Net.NetworkInformation;

namespace PingRT
{
    class FlowRate
    {
        private static IPGlobalProperties NicProperties = IPGlobalProperties.GetIPGlobalProperties();
        private static NetworkInterface[] NIC = NetworkInterface.GetAllNetworkInterfaces();
        private static long BeginSend, BeginRecieved, EndSend, EndRecieved, SendByte, RecievedByte;

        public FlowRate()
        {
            Timers.setInterval(() => Flow(), 1000);
        }

        public static long TotalFlowSend()
        {
            long x = 0;
            foreach (NetworkInterface adapter in NIC)
            {
                IPv4InterfaceStatistics ipv4Statistics = adapter.GetIPv4Statistics();
                x += (long)ipv4Statistics.BytesSent;
            }
            return x;
        }
        public static long TotalFlowReceived()
        {
            long y = 0;
            foreach (NetworkInterface adapter in NIC)
            {
                IPv4InterfaceStatistics ipv4Statistics = adapter.GetIPv4Statistics();
                y += (long)ipv4Statistics.BytesReceived;
            }
            return y;
        }

        public void Flow()
        {
            EndSend = TotalFlowSend();
            EndRecieved = TotalFlowReceived();
            SendByte = (EndSend - BeginSend) / 1024;
            RecievedByte = (EndRecieved - BeginRecieved) / 1024;
            BeginSend = EndSend;
            BeginRecieved = EndRecieved;
        }

        public static long GetSendByte()
        {
            return SendByte;
        }

        public static long GetRecievedByte()
        {
            return RecievedByte;
        }

        public static long GetEndSend()
        {
            return EndSend;
        }

        public static long GetEndRecieved()
        {
            return EndRecieved;
        }
        
    }
}
