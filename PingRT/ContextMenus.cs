﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Collections;

namespace PingRT
{
    class ContextMenus
    {
        
        ToolStripMenuItem SwitchItem = new ToolStripMenuItem();
        ToolStripMenuItem HostItem = new ToolStripMenuItem();
        ToolStripMenuItem IntervalItem = new ToolStripMenuItem();
        ToolStripMenuItem LimitItem = new ToolStripMenuItem();
        ToolStripMenuItem StartupItem = new ToolStripMenuItem();
        ToolStripMenuItem LedItem = new ToolStripMenuItem();
        ToolStripMenuItem NotifyItem = new ToolStripMenuItem();
        public static string[] CommonRef = new string[3];
        public ContextMenuStrip Create()
        {
            ContextMenuStrip NotifyMenu = new ContextMenuStrip();
            ToolStripMenuItem item;

            string[] NotifyMenuList = { "开启网络监测", "目标服务器", "检测频率", "开机启动", "日志", "关于", "退出","键盘灯告警" ,"通知栏告警","设定阈值"};
            string[] HostList = {ConfigINI.ProfileReadValue("common","h1"), ConfigINI.ProfileReadValue("common","h2"), ConfigINI.ProfileReadValue("common","h3")};
            string[] CommonHost = { "自定义主机", "编辑常用主机" };

            SwitchItem.Text = NotifyMenuList[0];
            SwitchItem.Checked = bool.Parse(Pinger.ConfigNodeOption[0].ToString());
            SwitchItem.Click += new EventHandler(SwitchItem_click);
            NotifyMenu.Items.Add(SwitchItem);

            LedItem.Text = NotifyMenuList[7];
            LedItem.Checked = bool.Parse(Pinger.ConfigNodeOption[2].ToString());
            LedItem.Click += new EventHandler(LedItem_click);
            NotifyMenu.Items.Add(LedItem);

            NotifyItem.Text = NotifyMenuList[8];
            NotifyItem.Checked = bool.Parse(Pinger.ConfigNodeOption[3].ToString());
            NotifyItem.Click += new EventHandler(NotifyItem_click);
            NotifyMenu.Items.Add(NotifyItem);

            foreach (string host in HostList)
            {
                ToolStripMenuItem HostSubItem = new ToolStripMenuItem(host);
                HostSubItem.Click += new EventHandler(HostItem_click);
                HostItem.DropDown.Items.Add(HostSubItem);
            }
            item = new ToolStripMenuItem();
            item.Text = CommonHost[0];
            item.Click += new EventHandler(InputHost_click);
            HostItem.DropDown.Items.Add(item);
            item = new ToolStripMenuItem();
            item.Text = CommonHost[1];
            item.Click += new EventHandler(inputCommonHost_click);
            HostItem.DropDown.Items.Add(item);
            HostItem.Text = NotifyMenuList[1];
            NotifyMenu.Items.Add(HostItem);


            IntervalItem.Text = NotifyMenuList[2];
            IntervalItem.Click += new EventHandler(IntervalItem_click);
            NotifyMenu.Items.Add(IntervalItem);

            LimitItem.Text = NotifyMenuList[9];
            LimitItem.Click += new EventHandler(LimitItem_click);
            NotifyMenu.Items.Add(LimitItem);

            StartupItem.Text = NotifyMenuList[3];
            StartupItem.Checked = AutoStartup.Check();
            StartupItem.Click += new EventHandler(StartupItem_click);
            NotifyMenu.Items.Add(StartupItem);

            item = new ToolStripMenuItem();
            item.Text = NotifyMenuList[4];
            NotifyMenu.Items.Add(item);

            item = new ToolStripMenuItem();
            item.Text = NotifyMenuList[5];
            item.Click += new System.EventHandler(ShowAbout_click);
            NotifyMenu.Items.Add(item);

            item = new ToolStripMenuItem();
            item.Text = NotifyMenuList[6];
            item.Click += new System.EventHandler(Exit_Click);
            NotifyMenu.Items.Add(item);

            return NotifyMenu;
        }

        

        void SwitchItem_click(object sender, EventArgs e)
        {
            SwitchItem.Checked = !SwitchItem.Checked;
            Pinger.ConfigNodeOption[0] = SwitchItem.Checked.ToString().ToLower();
            ConfigINI ini = new ConfigINI();
            ini.ProfileWriteValue("option", "enabled", SwitchItem.Checked.ToString().ToLower());
        }

        void HostItem_click(object sender, EventArgs e)
        {
            Pinger.ConfigNodeHost[0] = sender.ToString();
        }

        void InputHost_click(object sender, EventArgs e)
        {
            Form form = new Form();
            Label label = new Label();
            TextBox textBox = new TextBox();
            Button buttonOk = new Button();
            Button buttonCancel = new Button();
            label.Text = "输入域名或IP地址";
            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;
            label.SetBounds(9, 20, 372, 13);
            textBox.SetBounds(12, 36, 372, 20);
            buttonOk.SetBounds(228, 72, 75, 23);
            buttonCancel.SetBounds(309, 72, 75, 23);
            label.AutoSize = true;
            textBox.Anchor = textBox.Anchor | AnchorStyles.Right;
            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            form.Icon = Properties.Resources.network512_black;
            form.Text = "自定义主机";
            form.ClientSize = new Size(396, 107);
            form.Controls.AddRange(new Control[] { label, textBox, buttonOk, buttonCancel });
            form.ClientSize = new Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;
            DialogResult dialogResult = form.ShowDialog();
            if (dialogResult == buttonOk.DialogResult)
            {
                if (!Utils.IsIP(textBox.Text) && !Utils.IsDomain(textBox.Text))
                {
                    MessageBox.Show("请填写IP或者域名");
                }
                else
                {
                    Pinger.ConfigNodeHost[0] = textBox.Text;
                    ConfigINI ini = new ConfigINI();
                    ini.ProfileWriteValue("host", "h", textBox.Text.Trim());
                }
            }
        }

        void inputCommonHost_click(object sender, EventArgs e)
        {
            Form form = new Form();
            Label label = new Label();
            TextBox textBox = new TextBox();
            TextBox textBox1 = new TextBox();
            TextBox textBox2 = new TextBox();
            Button buttonOk = new Button();
            Button buttonCancel = new Button();
            label.Text = "输入域名或IP地址";
            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;
            label.SetBounds(9, 20, 372, 13);
            textBox.SetBounds(12, 36, 372, 20);
            textBox1.SetBounds(12, 80, 372, 20);
            textBox2.SetBounds(12, 124, 372, 20);
            buttonOk.SetBounds(228, 160, 75, 23);
            buttonCancel.SetBounds(309, 160, 75, 23);
            label.AutoSize = true;
            textBox.Anchor = textBox.Anchor | AnchorStyles.Right;
            textBox1.Anchor = textBox1.Anchor | AnchorStyles.Right;
            textBox2.Anchor = textBox2.Anchor | AnchorStyles.Right;
            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            form.Icon = Properties.Resources.network512_black;
            form.Text = "编辑常用主机";
            form.ClientSize = new Size(396, 200);
            form.Controls.AddRange(new Control[] { label, textBox, textBox1, textBox2, buttonOk, buttonCancel });
            form.ClientSize = new Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;
            DialogResult dialogResult = form.ShowDialog();
            if (dialogResult == buttonOk.DialogResult)
            {
                if (!Utils.IsIP(textBox.Text) && !Utils.IsDomain(textBox.Text))
                    MessageBox.Show("第一栏请填写IP或者域名");
                else if (!Utils.IsIP(textBox1.Text) && !Utils.IsDomain(textBox1.Text))
                    MessageBox.Show("第二栏请填写IP或者域名");
                else if (!Utils.IsIP(textBox2.Text) && !Utils.IsDomain(textBox2.Text))
                    MessageBox.Show("第三栏请填写IP或者域名");
                else
                {
                        ConfigINI ini = new ConfigINI();
                        ini.ProfileWriteValue("common", "h1", textBox.Text.Trim());
                        ini.ProfileWriteValue("common", "h2", textBox1.Text.Trim());
                        ini.ProfileWriteValue("common", "h3", textBox2.Text.Trim());
                }
            }
        }

        void LedItem_click(object sender, EventArgs e)
        {
            LedItem.Checked = !LedItem.Checked;
            Pinger.ConfigNodeOption[2] = LedItem.Checked.ToString().ToLower();
            ConfigINI ini = new ConfigINI();
            ini.ProfileWriteValue("option", "led", LedItem.Checked.ToString().ToLower());
        }

        void NotifyItem_click(object sender, EventArgs e)
        {
            NotifyItem.Checked = !NotifyItem.Checked;
            Pinger.ConfigNodeOption[3] = NotifyItem.Checked.ToString().ToLower();
            ConfigINI ini = new ConfigINI();
            ini.ProfileWriteValue("option", "notify", NotifyItem.Checked.ToString().ToLower());
        }

        void IntervalItem_click(object sender, EventArgs e)
        {
            Form form = new Form();
            Label label = new Label();
            TextBox textBox = new TextBox();
            Button buttonOk = new Button();
            Button buttonCancel = new Button();
            label.Text = "ping目的IP/域名的频率，单位秒";
            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;
            label.SetBounds(9, 20, 372, 13);
            textBox.SetBounds(12, 36, 372, 20);
            buttonOk.SetBounds(228, 72, 75, 23);
            buttonCancel.SetBounds(309, 72, 75, 23);
            label.AutoSize = true;
            textBox.Anchor = textBox.Anchor | AnchorStyles.Right;
            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            form.Icon = Properties.Resources.network512_black;
            form.Text = "刷新频率";
            form.ClientSize = new Size(396, 107);
            form.Controls.AddRange(new Control[] { label, textBox, buttonOk, buttonCancel });
            form.ClientSize = new Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;
            DialogResult dialogResult = form.ShowDialog();
            if (dialogResult == buttonOk.DialogResult)
            {
                if (!Utils.IsInt(textBox.Text.Trim()))
                {
                    MessageBox.Show("输入的值必须为正整数！");
                }
                else
                {
                    Pinger.ConfigNodeOption[1] = textBox.Text.Trim();
                    ConfigINI ini = new ConfigINI();
                    ini.ProfileWriteValue("option", "interval", textBox.Text.Trim());
                }
            }
        }

        void LimitItem_click(object sender, EventArgs e)
        {
            Form form = new Form();
            Label label = new Label();
            TextBox textBox = new TextBox();
            Button buttonOk = new Button();
            Button buttonCancel = new Button();
            label.Text = "延迟大于等于设定值时视为网络故障，单位毫秒";
            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;
            label.SetBounds(9, 20, 372, 13);
            textBox.SetBounds(12, 36, 372, 20);
            buttonOk.SetBounds(228, 72, 75, 23);
            buttonCancel.SetBounds(309, 72, 75, 23);
            label.AutoSize = true;
            textBox.Anchor = textBox.Anchor | AnchorStyles.Right;
            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            form.Icon = Properties.Resources.network512_black;
            form.Text = "设定阈值";
            form.ClientSize = new Size(396, 107);
            form.Controls.AddRange(new Control[] { label, textBox, buttonOk, buttonCancel });
            form.ClientSize = new Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;
            DialogResult dialogResult = form.ShowDialog();
            if (dialogResult == buttonOk.DialogResult)
            {
                if (!Utils.IsInt(textBox.Text.Trim()))
                {
                    MessageBox.Show("输入的值必须为正整数！");
                }
                else
                {
                    Pinger.ConfigNodeHost[1] = Convert.ToUInt16(textBox.Text.Trim());
                    ConfigINI ini = new ConfigINI();
                    ini.ProfileWriteValue("host", "limit", textBox.Text.Trim());
                }
            }
        }

        void StartupItem_click(object sender, EventArgs e)
        {
            StartupItem.Checked = !StartupItem.Checked;
            if (!AutoStartup.Set(StartupItem.Checked))
                MessageBox.Show("开机启动设置失败！");
        }

        void ShowAbout_click(object sender, EventArgs e)
        {
            Form form = new Form();
            Label label = new Label();
            Label labelVersion = new Label();
            //Label labelWebsite = new Label();
            LinkLabel labelWebsite = new LinkLabel();
            Label labelCopy = new Label();
            label.Text = "PingRT";
            label.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            label.Font = new System.Drawing.Font("Bauhaus 93", 30);
            label.SetBounds(LabelAutoAlign(label), 20, 372, 13);
            label.AutoSize = true;
            labelVersion.Text = "Version " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            labelVersion.Font = new System.Drawing.Font("", 12);
            labelVersion.SetBounds(LabelAutoAlign(labelVersion), 90, 372, 13);
            labelVersion.AutoSize = true;
            labelWebsite.Text = "http://git.oschina.net/1013/PingRT";
            labelWebsite.Font = new System.Drawing.Font("", 12);
            labelWebsite.SetBounds(LabelAutoAlign(labelWebsite), 150, 372, 13);
            labelWebsite.AutoSize = true;
            labelCopy.Text = "Copyright ©  2016";

            labelCopy.Font = new System.Drawing.Font("", 12);
            labelCopy.SetBounds(LabelAutoAlign(labelCopy), 170, 372, 13);
            labelCopy.AutoSize = true;
            form.Text = "关于";
            form.Icon = Properties.Resources.network512_black;
            form.ClientSize = new Size(396, 200);
            form.Controls.AddRange(new Control[] { label, labelVersion, labelWebsite, labelCopy });
            form.ClientSize = new Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            DialogResult dialogResult = form.ShowDialog();
        }

        void Exit_Click(object sender, EventArgs e)
        {
            NotifyEvent.Dispose();
            Application.Exit();
            System.Environment.Exit(0);
        }

        /// <summary>
        /// 关于页label水平绝对居中
        /// </summary>
        /// <param name="label"></param>
        /// <returns></returns>
        public static int LabelAutoAlign(Label label)
        {
            
            Graphics g = label.CreateGraphics();
            SizeF length = g.MeasureString(label.Text, label.Font);
            int width = Convert.ToInt16(length.Width);
            int algin = (300 - width) / 2;
            return algin;
        }

    }
}
