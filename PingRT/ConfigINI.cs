﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.IO;
using System.Collections;

namespace PingRT
{
    public class ConfigINI
    {
        //配置文件路径
        public static readonly string configpath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\pingrt_config.ini";


        [DllImport("kernel32")] // 写入配置文件的接口 
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);

        [DllImport("kernel32")] // 读取配置文件的接口 
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        [DllImport("Kernel32.dll")]
        private extern static int GetPrivateProfileStringA(string strAppName, string strKeyName, string sDefault, byte[] buffer, int nSize, string strFileName); 

        [DllImport("Kernel32.dll")]
        private static extern int GetPrivateProfileSection(string lpAppName, byte[] lpReturnedString, int nSize, string lpFileName);



        /// <summary>
        /// 判断配置文件是否存在，否则建立
        /// </summary>
        public void IniFiles()
        {
            FileInfo fileInfo = new FileInfo(configpath);
            if ((!fileInfo.Exists))
            {
                ProfileWriteValue("option", "enabled", "false");
                ProfileWriteValue("option", "interval", "1");     
                ProfileWriteValue("option", "led", "false");
                ProfileWriteValue("option", "notify", "false");
                ProfileWriteValue("host", "h", "www.baidu.com");
                ProfileWriteValue("host", "limit", "500");
                ProfileWriteValue("common", "h1", "www.baidu.com");
                ProfileWriteValue("common", "h2", "www.g.cn");
                ProfileWriteValue("common", "h3", "8.8.4.4");
            }
        }


        /// <summary>
        /// 将值写入指定节点指定key下
        /// </summary>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
 
        public void ProfileWriteValue(string section, string key, string value)
        {
            WritePrivateProfileString(section, key, value, configpath);
        }


        /// <summary>
        /// 读取指定节点指定key下的值
        /// </summary>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string ProfileReadValue(string section, string key)
        {
            StringBuilder sb = new StringBuilder(255);
            GetPrivateProfileString(section, key, "", sb, 255, configpath);
            return sb.ToString().Trim();
        }

        /// <summary> 
        /// 读取指定节点下的所有key 和value 
        /// </summary> 
        /// <param name="section"></param> 
        /// <returns></returns> 
        public static ArrayList GetIniSectionValue(string section)
        {
            byte[] buffer = new byte[5120];
            int rel = GetPrivateProfileSection(section, buffer, buffer.GetUpperBound(0), configpath);

            int iCnt, iPos;
            ArrayList arrayList = new ArrayList();
            string tmp;
            if (rel > 0)
            {
                iCnt = 0; iPos = 0;
                for (iCnt = 0; iCnt < rel; iCnt++)
                {
                    if (buffer[iCnt] == 0x00)
                    {
                        tmp = System.Text.ASCIIEncoding.Default.GetString(buffer, iPos, iCnt - iPos).Trim();
                        iPos = iCnt + 1;
                        if (tmp != "")
                            arrayList.Add(tmp);
                    }
                }
            }
            return arrayList;
        }

        /// <summary> 
        /// 读取指定节点下的所有key的value 
        /// </summary> 
        /// <param name="section"></param> 
        /// <returns></returns> 
        public static ArrayList GetIniValue(string section)
        {
            byte[] buffer = new byte[5120];
            int rel = GetPrivateProfileSection(section, buffer, buffer.GetUpperBound(0), configpath);

            int iCnt, iPos;
            ArrayList arrayList = new ArrayList();
            string tmp;
            if (rel > 0)
            {
                iCnt = 0; iPos = 0;
                for (iCnt = 0; iCnt < rel; iCnt++)
                {
                    if (buffer[iCnt] == 0x00)
                    {
                        tmp = System.Text.ASCIIEncoding.Default.GetString(buffer, iPos, iCnt - iPos).Trim();
                        iPos = iCnt + 1;
                        string[] abc = tmp.Split('=');

                        if (tmp != "")
                            arrayList.Add(abc[1]);
                    }
                }
            }
            return arrayList;
        }
  

        /// <summary> 
        ///  获取指定节点的所有KEY的名称 
        /// </summary> 
        /// <param name="sectionName"></param> 
        /// <returns></returns> 
        public ArrayList ReadKeys(string sectionName)
        {

            byte[] buffer = new byte[5120];
            int rel = GetPrivateProfileStringA(sectionName, null, "", buffer, buffer.GetUpperBound(0), configpath);

            int iCnt, iPos;
            ArrayList arrayList = new ArrayList();
            string tmp;
            if (rel > 0)
            {
                iCnt = 0; iPos = 0;
                for (iCnt = 0; iCnt < rel; iCnt++)
                {
                    if (buffer[iCnt] == 0x00)
                    {
                        tmp = System.Text.ASCIIEncoding.Default.GetString(buffer, iPos, iCnt - iPos).Trim();
                        iPos = iCnt + 1;
                        if (tmp != "")
                            arrayList.Add(tmp);
                    }
                }
            }
            return arrayList;
        }

        
        /// <summary> 
        /// 读取指定节点下的指定key的value返回string 
        /// </summary> 
        /// <param name="section"></param> 
        /// <param name="key"></param> 
        /// <returns></returns> 
        public string GetIniKeyValueForStr(string section, string key)
        {
            if (section.Trim().Length <= 0 || key.Trim().Length <= 0) return string.Empty;
            StringBuilder strTemp = new StringBuilder(256);
            GetPrivateProfileString(section, key, string.Empty, strTemp, 256, configpath);
            return strTemp.ToString().Trim();
        } 


    }
}
