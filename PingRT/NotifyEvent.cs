﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Runtime.InteropServices;


namespace PingRT
{
    public class NotifyEvent
    {
        public static NotifyIcon IconEvent;
        private static int FlagCounter=1;

        public NotifyEvent()
        {
            IconEvent = new NotifyIcon();
            IconEvent.ContextMenuStrip = new ContextMenus().Create();
        }

        public void Display()
        {
            IconEvent.Visible = true;
            IconEvent.Icon = Properties.Resources.network512_white;
        }
        public void Display(string ico)
        {
            IconEvent.Visible = true;
            IconEvent.Icon = IconBuild.getIcon(ico);
        }

        public static void DefaultIcon()
        {
            IconEvent.Icon = Properties.Resources.network512_white;
        }

        public static void Dispose()
        {
            IconEvent.Visible = false;
            IconEvent.Icon = null;
            IconEvent = null;
        }

        public static void SetNotifyText(string text)
        {
            IconEvent.Text = text;
        }

        public static void AlternateNotifyIcon()
        {
            FlagCounter++;
            if(FlagCounter%2==0)
                IconEvent.Icon = Properties.Resources.network512_black;
            else
                IconEvent.Icon = Properties.Resources.network512_white;
        }

    }
}
