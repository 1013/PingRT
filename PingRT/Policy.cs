﻿using System;
using System.Collections.Generic;
using System.Collections;

namespace PingRT
{
    class Policy
    {
        public static void  AlarmPolicy()
        {
            AlarmPolicyArgs APA = new AlarmPolicyArgs();
            APA.AlarmPolicyKBEvent += new AlarmPolicyArgs.AlarmPolicy(OnAlarmPolicyKBEvent);
            APA.AlarmPolicyIconEvent += new AlarmPolicyArgs.AlarmPolicy(OnAlarmPolicyIconEvent);
            APA.AlarmPolicyRun();
        }


        private static void OnAlarmPolicyKBEvent(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(System.DateTime.Now.ToString()+"触发了键盘灯事件");
            KBEvent.pressScroll();
        }

        private static void OnAlarmPolicyIconEvent(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(System.DateTime.Now.ToString() + "触发了图标事件");
            NotifyEvent.AlternateNotifyIcon();
        }

    }
}
